# |/usr/bin/env python3

"Sort a list of songs based on their number of plays."

import sys


def order_items(songs: list, i: int, gap: int) -> list:
    n = len(songs)

    while i + gap < n:
        j = i
        while j >= 0 and songs[j][1] < songs[j + gap][1]:
            songs[j], songs[j + gap] = songs[j + gap], songs[j]
            j -= gap

        i += 1

    return songs


# Example usage:
songs = [("El Mambo", 10000), ("desesperados", 35000), ("casualidad", 500000), ("te robe", 12005)]
i = 0
gap = 2

ordered_songs = order_items(songs, i, gap)
print(ordered_songs)


def sort_music(canciones, reproducciones):
    diccionario_combinado=dict(zip(canciones,reproducciones))
    diccionario_ordenado=dict(sorted(diccionario_combinado.items(),key=lambda x:x[1], reverse=True))
    canciones_ordenadadas=list(diccionario_ordenado.keys())


    return canciones_ordenadadas


def create_dictionary(arguments):
   if len(arguments) % 2 !=0:
       raise ValueError("la lista de argumentos debe tener longitud par")

   diccionario_canciones_reproducciones = dict(zip(arguments[::2],arguments[1::2]))


    return diccionario_canciones_reproducciones


def main():
    args = sys.argv[1:]

    if len(args) % 2 != 0 or len(args) == 0:
        sys.exit("Error: Debes proporcionar pares de canción y número de reproducciones.")

    songs: dict = create_dictionary(args)

    sorted_songs: list = sort_music(list(songs.items()))

    sorted_dict: dict = dict(sorted_songs)
    print(sorted_dict)


if __name__ == '__main__':
    main()